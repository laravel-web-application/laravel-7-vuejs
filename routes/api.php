<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Ambil Semua Data Artikel
Route::get('articles', 'ArticleController@index');// Membuat Artikel Baru
Route::post('article', 'ArticleController@store');// Mengubah Artikel
Route::put('article', 'ArticleController@store');// Menghapus Artikel
Route::delete('article/{id}', 'ArticleController@destroy');
